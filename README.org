#+Title: John Conway's Game of Life Implemented in Python

* Dependencies
This project uses [[https://github.com/python-poetry/poetry][poetry]] for dependency management. At the moment there are no dependencies besides
pytest to run the unit tests contained within the tests folder.

If you want to install poetry, I recommend doing so via [[https://pypi.org/project/pipx/][pipx]], which can be
installed via pip on all platforms or through apt on Debian/Ubuntu based systems

To install poetry dependencies, run the following in the project folder

#+begin_src sh
poetry install
#+end_src

* How to Run
#+begin_src sh
python game_of_life.py -h
#+end_src
Displays a help menu with the following information:

#+begin_src
usage: game_of_life.py [-h] filepath

positional arguments:
  filepath    location of file to load from

optional arguments:
  -h, --help  show this help message and exit
#+end_src

To run the program just supply a filepath to a file that follows [[Creating Your Own Starting States][these guidelines]]:
#+begin_src sh
python game_of_life.py glider.txt
#+end_src

* Creating Your Own Starting States
When parsing files the program only looks for ones and zeros. Ones are considered live cells, and zeros are dead cells.
However the characters are arranged is exactly how the board state will be interpreted, nothing else required.

Examples for valid files are in the examples folder in the main project directory

* Testing
To run the included tests, use the following command
from the main project directory:
#+begin_src sh
pytest --color=yes tests/test_game_of_life.py
#+end_src

