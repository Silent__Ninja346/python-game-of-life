from game_of_life import __version__
from game_of_life.main import next_board_state, DIRECTIONS, dead_state


def test_version():
    assert __version__ == "0.1.0"


def test_neighbor_count():
    init_state = [[1, 1, 1], [1, 0, 1], [1, 1, 1]]
    expected_neighbors = 8
    actual_neighbors = 0

    width = len(init_state)
    height = len(init_state[0])

    for direction in DIRECTIONS:
        check = (
            ((1 + direction[0]) % width),
            ((1 + direction[1]) % height),
        )
        if init_state[check[0]][height - 1 - check[1]]:
            actual_neighbors += 1

    assert expected_neighbors == actual_neighbors


def test_dead_stay_dead():
    init_state = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
    expected_next_state = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]

    actual_next_state = next_board_state(init_state)

    assert expected_next_state == actual_next_state


def test_alive_dies():
    # 0 0 0
    # 0 1 1
    # 0 0 0
    init_state = [[0, 0, 0], [0, 1, 0], [0, 1, 0]]

    # 0 0 0
    # 0 0 0
    # 0 0 0
    expected_next_state = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]

    actual_next_state = next_board_state(init_state)

    assert expected_next_state == actual_next_state


def test_reproduction():
    # 0 0 0
    # 1 1 1
    # 0 0 0
    init_state = [[0, 1, 0], [0, 1, 0], [0, 1, 0]]

    # 1 1 1
    # 1 1 1
    # 1 1 1
    expected_next_state = [[1, 1, 1], [1, 1, 1], [1, 1, 1]]

    actual_next_state = next_board_state(init_state)

    assert expected_next_state == actual_next_state


def test_reproduction_4x4():
    # 0 0 0 0
    # 1 1 1 0
    # 0 0 0 0
    # 0 0 0 0
    init_state = [[0, 0, 1, 0], [0, 0, 1, 0], [0, 0, 1, 0], [0, 0, 0, 0]]

    # 0 1 0 0
    # 0 1 0 0
    # 0 1 0 0
    # 0 0 0 0
    expected_next_state = [[0, 0, 0, 0], [0, 1, 1, 1], [0, 0, 0, 0], [0, 0, 0, 0]]

    actual_next_state = next_board_state(init_state)

    assert expected_next_state == actual_next_state


def test_toad():
    """
    000000
    000000
    001110
    011100
    000000
    000000
    """
    init_state = [
        [0, 0, 0, 0, 0, 0],
        [0, 0, 1, 0, 0, 0],
        [0, 0, 1, 1, 0, 0],
        [0, 0, 1, 1, 0, 0],
        [0, 0, 0, 1, 0, 0],
        [0, 0, 0, 0, 0, 0],
    ]

    """
    000000
    000100
    010010
    010010
    001000
    000000
    """
    expected_next_state = [
        [0, 0, 0, 0, 0, 0],
        [0, 0, 1, 1, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [0, 0, 0, 0, 1, 0],
        [0, 0, 1, 1, 0, 0],
        [0, 0, 0, 0, 0, 0],
    ]

    actual_next_state = next_board_state(init_state)

    assert expected_next_state == actual_next_state


def test_open_file():
    filepath = "tests/toad.txt"
    with open(filepath) as f:
        lines = [line.rstrip() for line in f]

    width = len(lines[0])
    height = len(lines)
    state = dead_state(width, height)

    for y in range(0, height):
        for x in range(0, width):
            state[x][height - 1 - y] = int(lines[y][x])

    expected_state = [
        [0, 0, 0, 0, 0, 0],
        [0, 0, 1, 0, 0, 0],
        [0, 0, 1, 1, 0, 0],
        [0, 0, 1, 1, 0, 0],
        [0, 0, 0, 1, 0, 0],
        [0, 0, 0, 0, 0, 0],
    ]

    assert state == expected_state
